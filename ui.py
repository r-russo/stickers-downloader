import tkinter as tk
from tkinter import filedialog
from tkinter import scrolledtext
from tkinter import messagebox
from tkinter import ttk
import os
from urllib.request import urlretrieve
import PIL
from PIL import Image

from stickers import get_img_url


class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()
        self.create_widgets()

    def create_widgets(self):
        self.master.title("Stickers")
        self.pack(fill=tk.BOTH, expand=True)

        self.links = scrolledtext.ScrolledText(self)
        self.links.pack(fill=tk.BOTH, expand=True)

        frame_folder = tk.Frame(self)
        lblfolder = tk.Label(frame_folder, text="Folder")
        lblfolder.pack(side=tk.LEFT, anchor=tk.N)
        self.foldervar = tk.StringVar()
        self.folder = tk.Entry(frame_folder, textvariable=self.foldervar)
        self.foldervar.set(os.getcwd())
        self.folder.pack(side=tk.LEFT, anchor=tk.N, fill=tk.X, expand=True)
        btnfolder = tk.Button(frame_folder, text="Select",
                              command=self.change_directory)
        btnfolder.pack(side=tk.LEFT, anchor=tk.N)
        frame_folder.pack(fill=tk.X)

        self.btndownload = tk.Button(self, text="Download",
                                     command=self.download)
        self.btndownload.pack()

        self.progress = tk.IntVar()
        self.progress.set(100)
        self.progress_bar = ttk.Progressbar(self, variable=self.progress)
        self.progress_bar.pack(fill=tk.X, pady=10)
        self.status = tk.StringVar()

        lblStatus = tk.Label(textvariable=self.status)
        self.status.set("Ready")
        lblStatus.pack()

    def change_directory(self):
        folder = filedialog.askdirectory()
        self.foldervar.set(folder)

    def download(self):
        links = self.links.get("1.0", "end-1c").splitlines()
        self.btndownload['state'] = "disabled"
        self.links['state'] = "disabled"
        self.folder['state'] = "disabled"
        try:
            for i, link in enumerate(links):
                self.status.set("(%d/%d) " % (i+1, len(links)) +
                                "Retrieving stickers URLs...")
                self.update()
                title, stickers = get_img_url(link)

                self.status.set("(%d/%d) " % (i+1, len(links)) +
                                "Downloading " + title)

                path = os.path.join(self.folder.get(), title)
                if not os.path.exists(path):
                    os.makedirs(path)
                else:
                    messagebox.showwarning(
                        "Directory error",
                        "Can't create directory for %s. Skipping..." % title
                    )
                    continue

                self.progress_bar['maximum'] = len(stickers)
                self.progress.set(0)
                for ix, st in enumerate(stickers):
                    urlretrieve(st, os.path.join(title, 'tmp.png'))
                    im = Image.open(os.path.join(title, 'tmp.png'))
                    ratio = 512/max(im.size)
                    x = round(ratio*im.size[0])
                    y = round(ratio*im.size[1])
                    im = im.resize((x, y), PIL.Image.LANCZOS)
                    im.save(os.path.join(title, '%02d.png' % (ix+1)))
                    self.progress.set(self.progress.get() + 1)
                    self.update()
                os.remove(os.path.join(title, 'tmp.png'))
        except Exception as e:
            messagebox.showerror("Something went wrong", str(e))
        finally:
            self.btndownload['state'] = "normal"
            self.links['state'] = "normal"
            self.folder['state'] = "normal"
            self.status.set("Ready")


if __name__ == "__main__":
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()

