from bs4 import BeautifulSoup
from urllib.request import urlretrieve
from PIL import Image

import PIL
import os
import sys
import requests
import re


def get_img_url(url):
    response = requests.get(url)

    s = BeautifulSoup(response.text, 'html.parser')

    stickers = []

    if "store.line.me" in url:
        sticker_class = 'mdCMN09Image FnPreview'
        tags = s.find_all('span', sticker_class)
        regex = re.compile('http.*(?=;c)')
        print(len(tags))
        for t in tags:
            img = regex.findall(t.attrs['style'])[0]
            stickers.append(img)

    elif "yabeline.tw" in url:
        sticker_class = 'stickerSub'
        tags = s.find_all('li', sticker_class)
        for i in tags:
            img = i.find('img')['src']
            stickers.append(img)
    else:
        print("Domain not supported")
        sys.exit(1)

    title = s.title.string
    title = title[:title.find(' – ')]

    return title, stickers


def download_stickers(stickers, title):
    if not os.path.exists(title):
        os.makedirs(title)
    else:
        ix = 0
        while os.path.exists(title + f" ({ix})"):
            ix += 1
        title += f" ({ix})"
        print(f'Directory already exists. Downloading stickers at {title}')
        os.makedirs(title)

    print("Downloading and resizing stickers...")
    for ix, st in enumerate(stickers):
        print('[%d/%d]' % (ix+1, len(stickers)), end='\r')
        urlretrieve(st, os.path.join(title, 'tmp.png'))
        im = Image.open(os.path.join(title, 'tmp.png'))
        ratio = 512/max(im.size)
        x = round(ratio*im.size[0])
        y = round(ratio*im.size[1])
        im = im.resize((x, y), PIL.Image.LANCZOS)

        im.save(os.path.join(title, '%02d.png' % (ix+1)))
    os.remove(os.path.join(title, 'tmp.png'))
    print()


if __name__ == "__main__":
    if len(sys.argv) >= 2:
        if sys.argv[1] == '-f':
            with open(sys.argv[2], 'r') as f:
                sites = f.read().splitlines()
        else:
            sites = [sys.argv[1]]
    else:
        print('LINE stickers downloader for Telegram')
        print('stickers.py [-f] URL/File')
        sys.exit(1)

    for site in sites:
        title, stickers = get_img_url(site)
        download_stickers(stickers, title)
